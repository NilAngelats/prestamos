$(window).on("load", function(){ 
    var range = $("#range").attr("value");
    $("#demo").html(range);
    $(".slide").css("width", "50%");
    $(document).on('input change', '#range', function() {
      $('#demo').html( $(this).val() );
      var slideWidth = $(this).val() * 100 / 50000;
      
      $(".slide").css("width", slideWidth + "%");
  });
  });
  
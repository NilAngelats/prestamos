
const form = document.querySelector("#formulario");
const currency = document.querySelector("#moneda");
const typeCrypto = document.querySelector("#criptomonedas");
const result = document.querySelector("#resultado");

const cryptoObject = {
    currency: "",
    cryptocurrency: ""
}

const getCryptocurrencies = cryptocurrencies => new Promise( resolve => {
    resolve(cryptocurrencies)
})

document.addEventListener("DOMContentLoaded", () => {
    apiCrypto();
    form.addEventListener("submit", cryptocurrenciesValue);
    currency.addEventListener("change", readValue);
    typeCrypto.addEventListener("change", readValue);

});

function apiCrypto () {
    const url = 'https://min-api.cryptocompare.com/data/top/mktcapfull?limit=10&tsym=USD';
    fetch(url)
        .then(response => response.json())
        .then(data => getCryptocurrencies(data.Data))
        .then(cryptocurrencies => selectCryptocurrencies(cryptocurrencies))
};

function selectCryptocurrencies (cryptocurrencies) {
    cryptocurrencies.forEach(cryptocurrency => {
        const {Name, FullName} = cryptocurrency.CoinInfo;
        const option = document.createElement('option');
        option.value = Name;
        option.textContent = FullName;
        typeCrypto.appendChild(option);

    })
};

function readValue (e) {
    cryptoObject[e.target.name] = e.target.value;
};

function cryptocurrenciesValue (e) {
    e.preventDefault();
    const {currency, cryptocurrency} = cryptoObject;
    if(currency === "" || cryptocurrency === "") {
        showUpAlert("Please fill in all required fields", "error");
        return;
    }
    getDataFromApi()
};

function showUpAlert (message, type) {
    const alert = document.querySelector(".alert");
    if(!alert) {
        const div = document.createElement("div");
        div.classList.add("alert")
        if (type === "error") {
            div.classList.add("error")
        }
        div.textContent = message;
        form.appendChild(div);
        setTimeout(() => {
            div.remove()
        }, 2500);
    }
};

function getDataFromApi () {
    const {currency, cryptocurrency} = cryptoObject;

    const url = `https://min-api.cryptocompare.com/data/pricemultifull?fsyms=${cryptocurrency}&tsyms=${currency}`
    fetch(url)
        .then(response => response.json())
        .then(data => setInformation(data.DISPLAY[cryptocurrency][currency]))
};

function setInformation (data) {
    spinner ();
    setTimeout(() => {
        cleanHTML ();        
        const {cryptocurrency} = cryptoObject
        const {PRICE, HIGHDAY, LOWDAY, CHANGEPCT24HOUR, LASTUPDATE} = data;
        const par = document.createElement("p");
        par.innerHTML = `
            <p><span>${cryptocurrency}</span> price: <span>${PRICE}</span></p><br>
            <p>Highest price today: <span>${HIGHDAY}</span></p><br>
            <p>Lowest price today: <span>${LOWDAY}</span></p><br>
            <p><span>${cryptocurrency}</span> returns 24 hours: <span>${CHANGEPCT24HOUR}%</span></p><br>
            <p><span>${cryptocurrency}</span> update: <span>${LASTUPDATE}</span></p><br>
        `;
        result.appendChild(par);
    }, 2700);
}

function cleanHTML () {
    while(result.firstChild) {
        result.removeChild(result.firstChild)
    }
};

function spinner () {
    cleanHTML();

    const spinner = document.createElement("div");
    spinner.classList.add("sk-folding-cube");
    spinner.innerHTML = `
        <div class="sk-cube1 sk-cube"></div>
        <div class="sk-cube2 sk-cube"></div>
        <div class="sk-cube4 sk-cube"></div>
        <div class="sk-cube3 sk-cube"></div>
    `
    result.appendChild(spinner)
}
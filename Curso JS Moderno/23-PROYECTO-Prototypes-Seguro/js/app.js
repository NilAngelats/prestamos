//Variables
const marca = document.querySelector("#marca");
const year = document.querySelector("#year");
const tipo = document.querySelector("input[name=tipo]");
const formulario = document.querySelector("#cotizar-seguro");
const resultado = document.querySelector("#resultado");

//Prototypes
function Seguro (marca, year, tipo) {
    this.marca = marca;
    this.year = year;
    this.tipo = tipo;
}

Seguro.prototype.calcularSeguro = function () {
    /*
    Americano 1.15
    Asiatico 1.05
    Europeo 1.35
    */
    let cantidad;
    const base = 2000;
    switch(this.marca.value) {
        case "1":
            cantidad = base *1.15;
            break;
        case "2":
            cantidad = base *1.05;
            break;
        case "3":
            cantidad = base *1.35;
            break;
        default:
            break;
    }
    let diferencia = new Date().getFullYear() - this.year.value
    cantidad -= ((diferencia * 3) * cantidad) / 100;
    const tipoChecked = document.querySelector("input[name=tipo]:checked").value
    if (tipoChecked === "basico") {
        cantidad *= 1.30;
    }else {
        cantidad *= 1.50;
    }
    return cantidad;
}

function UI () {

}

UI.prototype.mostrarMensaje = (mensaje, tipo) => {
    const div = document.createElement("div");
    div.classList.add("mensaje", "mt-10");
    div.textContent = mensaje;
    if (tipo === "error") {
        div.classList.add("error")
    } else {
        div.classList.add("correcto")
    }
    formulario.insertBefore(div, resultado);
    setTimeout(() => {
        div.remove()
    }, 1500);
}

UI.prototype.cargando = () => {
    const spinner = document.querySelector("#cargando");
    spinner.style.display = "block"
    setTimeout(() => {
        spinner.style.display = "none"
    }, 1500);
}

UI.prototype.mostrarSeguro = (total) => {
    const div = document.createElement("div");
    let texto;
    switch(this.marca.value) {
        case "1":
            texto = "Americano";
            break;
        case "2":
            texto = "Asiatico";
            break;
        case "3":
            texto = "Europeo";
            break;
    }
    div.innerHTML = `
        <p class="header">Seguro</p>
        <p class="font-bold">Total: <span class="font-normal"> ${total}€</span></p>
        <p class="font-bold">Marca: <span class="font-normal"> ${texto}</span></p>
        <p class="font-bold">Año: <span class="font-normal"> ${year.value}</span></p>
        <p class="font-bold">Tipo: <span class="font-normal"> ${tipo.value}</span></p>
    `
    
    setTimeout(() => {
            resultado.appendChild(div);
    }, 1500);
}

const ui = new UI;

loadEventListener ();

function loadEventListener () {
    formulario.addEventListener("submit", cotizarSeguro);
    document.addEventListener ("DOMContentLoaded", () => {
        const data = new Date();
        const max = data.getFullYear();
        let min = max - 20;
        for(i=max; i>min; i--) {
            const option = document.createElement("option");            
            option.textContent = i;
            option.value = i;
            year.appendChild(option);
        }
    })
}

function cotizarSeguro (e) {
    e.preventDefault();
    if(marca.value === "" || year.value === "" || tipo === "") {
        ui.mostrarMensaje("Todos los campos son obligatorios", "error");
        const resultadoDiv = document.querySelector("#resultado div");
        if (resultadoDiv != null) {
            resultadoDiv.remove()
        }
        return;
    }
    const seguro = new Seguro (marca, year, tipo);
    ui.mostrarMensaje("Cotizando...", "exito");
    ui.cargando();
    const resultadoDiv = document.querySelector("#resultado div");
    if (resultadoDiv != null) {
        resultadoDiv.remove()
    }
    let total = seguro.calcularSeguro();
    
    ui.mostrarSeguro(total, seguro);

}

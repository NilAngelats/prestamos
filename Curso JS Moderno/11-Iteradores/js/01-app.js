var slider = document.getElementById("myRange");
var output = document.getElementById("demo");
var style = document.querySelector('[data="test"]');

setData(slider.value);

slider.oninput = function() {
    setData(this.value);
}

function setData(x){
    
    style.innerHTML = "#myRange::-webkit-slider-thumb { width: " + (x < 20 ? 20 : x)  + "px !important; height: " + (x < 20 ? 20 : x) + "px !important; }";}
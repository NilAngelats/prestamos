// GENERADORES
// Es una funció que torna un iterador. Sempre *. Yield és dels generadors i serveix per especificar quins elements es poden iterar. 
function *crearGenerador() {
    yield 1;
    yield 'Juan';
    yield 3+3;
    yield true;
}
crearGenerador();
const iterador = crearGenerador();

console.log(iterador);

console.log(crearGenerador);

// Podem veure que no es el mateix imprimir la funció si la guardem en una variable, o bé, si la imprimim directament.
// Té lògica ja que en una imprimim la funcionalitat i l'altre només el contingut, mira la diferencia de les ().
// Podem veure el prototype del generador que hi ha un mètode .next() que fa que es desperti, ja que hem vist que que la funció està suspended.

// console.log(iterador.next().value);
// console.log(iterador.next().done);
// console.log(iterador.next().value); // Aquí et podries esperar que t imprimis Juan, però hem de pensar que per cada next salta una linia.
// console.log(iterador.next());
// console.log(iterador.next());
// console.log(iterador.next());

function *generadorCarrito (carrito) {
    for (let i = 0; i < carrito.length; i++) {
        yield carrito[i];
    }
}

// Hem de posar yield per extreure els valors de carrito

const carrito = ['Producte1', 'Producte2', 'Producte3'];

const iterador2 = generadorCarrito(carrito);

console.log(iterador2.next())
console.log(iterador2.next())
console.log(iterador2.next())
console.log(iterador2.next())


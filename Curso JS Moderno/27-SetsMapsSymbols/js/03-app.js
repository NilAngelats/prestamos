//ITERADORES

const ciudades = ['Londres', 'New York','Madrid', 'Paris'];
const ordenes = new Set([123,231,131,102]);
const datos = new Map();

datos.set('nombre', 'Juan');
datos.set('profesion', 'Crac');

// Recordem que totes aquestes dades son iterables. 

//Iterem per entry
for (let entry of ciudades.entries()) {
    console.log(entry);
}

for (let entry of ordenes.entries()) {
    console.log(entry);
}

for (let entry of datos.entries()) {
    console.log(entry);
}

// Podem veure que per ciudades hi ha index-valors ARRAY, ordenes els dos valors són iguals SET i per datos hi ha clau-valor MAP.
// Ens hem de fixar que ens torna tots els resultats en forma d'array.

// Iterem per value
for (let value of ciudades.values()) {
    console.log(value);
}

for (let value of ordenes.values()) {
    console.log(value);
}

for (let value of datos.values()) {
    console.log(value);
}

// Iterem per a key
for (let keys of ciudades.keys()) {
    console.log(keys);
}
for (let keys of ordenes.keys()) {
    console.log(keys);
}
for (let keys of datos.keys()) {
    console.log(keys);
}

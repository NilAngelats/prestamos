// FUNCIONAMENT DEL ITERADORS
// Això és una funció que retorna una altra funció
function crearIterador (carrito) {
    let i = 0;

    return {
        siguiente: () => {
            const fin = (i >= carrito.length);
            const valor = !fin ? carrito[i++] : undefined ;
            // És  i++ pq ja determinem i = 0 en abans del return.
            return {
                fin,
                valor
            }
        }
    }
}

const carrito = ['Producte 1', 'Preducte 2', 'Producte 3']

// Per aquest motiu guardem la funció en una variable, pel carrito i aquesta variable ens generarà la funció siguiente que haurem d'aplicar. Ho guardem per poder cridar al métode.
const recorrerCarrito = crearIterador(carrito);

// En la línia anterior hem generat la funció siguiente, o pot ser li hauriem de dir mètode.
console.log(recorrerCarrito.siguiente())

// Hem de repatir el console.log per tal de que iteri. Bàsicament estem veient com funcionen els iteradors.
console.log(recorrerCarrito.siguiente())
console.log(recorrerCarrito.siguiente())
console.log(recorrerCarrito.siguiente())



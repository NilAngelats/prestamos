// Primer assignem les variables

const carrito = document.querySelector('#carrito');
const contenedorCarrito = document.querySelector('#lista-carrito tbody');
const listaCursos = document.querySelector('#lista-cursos');      // S'agafa l'id que engloba tots els botons d'agregar. Pq sino, només et deixarà fer el primer agregar
const vaciarCarritoBtn = document.querySelector('#vaciar-carrito')
let articulosCarrito = []

// Com que hi haurà molts eventos i funcions, englobem els eventos en un.

cargarEventListeners();

function cargarEventListeners () {
    // Afegir curs al clicar
    listaCursos.addEventListener ('click', agregarCurso);

    // Esborrar curs al clicar
    carrito.addEventListener ('click', coursedelete);

    // Buidar el carrito al clicar
    vaciarCarritoBtn.addEventListener('click', emptying);
}


// Funcions

function agregarCurso(e) {              // Aquí posem e com argument per determinar tota la informació de l'event. Si t'hi fixes les altres funcions no són funcions d'event.
    e.preventDefault();    
    if (e.target.classList.contains('agregar-carrito')) {       //Aquí estem accedint al botó, però hem d'anar a la informació i que la pugi al carrito.
        const contenidor = e.target.parentElement.parentElement;
        readinfo(contenidor);
    }
}


function coursedelete (e) {
    if(e.target.classList.contains('borrar-curso')) {   //Ja tenim especificat el lloc on es produirà la funcionalitat.
        // Hem d'aconseguir identificar que estem clicant, quin curs. Per això haurem d'aconseguir el id del curs.
        // També hem d'identificar per a poder dir que volem borrar. Ja que si cliquem en una X i posem borrar el primer child, pot ser estem borrant un curs que no volem borrar. Per tant, per a borrar el que toca ho hem de fer a través de la id.
        const courserId = e.target.getAttribute('data-id');
        // Eliminem el curs per la id igualant el carrito a al mateix carrito però sense el curs. Com en python quan fem un filtratge per condició data=data[]. Fem !== pq filter es queda els true, llavors volem quedar-nos tots els que no tenen la id d'eliminació.
        articulosCarrito = articulosCarrito.filter(course => course.id !== courserId)
        // Filter se'n va a articulocarrito i passa cada curs per course i ho compara per courseId. Si dona true ho guarda, si dona flase descarta.
        addinfo();
        // Llavors com que articuloCarrito ja està sense el curs que voliem eliminar, ara volem pujar aquest carrito net a HTML altre cop, per això utilitzem la funciò addinfo().
    }
}


function emptying (e) {
        // En aquest cas en la variable vaciarcarritobtn ja hem especificat on s'ha de clicar, i al fer el event per aquella variable, ja esta concretat on s'ha de clicar. Però per a les altres variables que hem hagut d'utilitzar if, és perquè hi havia més d'un botó per a clicar, llavors has d'agafar el marc gros i després dins la funció determinar el lloc. Ja que si en la variable que fas passar per l'event, aquest només farà el primer dels que comparteixen class. Per això en el if dius, si dins del marc total cliquem un botó i aquest conté la class que volem, gas amb la funció. D'aquesta manera no només fa el primer, sinó tots els que estiguin dins del marc.
    articulosCarrito = [];      // Pel motiu explicat a baix de tot assignem una array buida a articulosCarrito.

    cleanHTML();
}


function readinfo(course) {
    // console.log(course)

    // Creem un objecte on guardar la info
    const info = {
        image: course.querySelector('img').src,
        title: course.querySelector('h4').textContent,
        price: course.querySelector('.precio span').textContent,
        id: course.querySelector('a').getAttribute('data-id'),
        count: 1
    };
    // Comprovem si hi ha algun igual abans. Hem de pensar que .some és per arrays, pero la part bona és que hem creat una array, articulosCarrito. Cal recordar que el parametre és informatiu, que itera i que la primera comprovació és de l'element entrat.
    // Hem de pensar que pot ser tenim un curs en el carrito, i abans de posar el curs llegit en la funció anterior, volem comprovar si és el mateix curs, amb la qual cosa la quantitat hauria de canviar.
    // Passi el que passi, ha de sortir un element per poder guardar-lo a carrito. per això utilitzem map.
    const exists = articulosCarrito.some(course => course.id === info.id)
    if (exists) {
        const course = articulosCarrito.map((course) => {
            if (course.id === info.id) {
                course.count ++;
                return course;
            }else {
                return course;
            };
        });
        articulosCarrito = [...course];
    }else {
        // És important posar aquesta variable i console dins de la funció per referenciar a info.
        articulosCarrito = [...articulosCarrito, info];
    }
        
    addinfo ();
};


function addinfo () {
    cleanHTML();

    articulosCarrito.forEach(course => {
        const {image, title, price, count, id} = course;        // Fem un destructering de l'objecte per tal de tenir més eficiència.
        const row = document.createElement('tr')
        row.innerHTML = `
            <td>
                <img src ="${image}" width="150">
            </td>
            <td> ${title} </td>
            <td> ${price} </td>
            <td> ${count} </td>
            <td>
                <a href="#" class="borrar-curso" data-id="${id}"> X </a>
            </td>
        `;                          // Aquí no podem posar textContent
        contenedorCarrito.appendChild(row);
    });
    
}

function cleanHTML () {     // Volem netejar el contenidor, aj que sinó aquest s'anirà emplenant i repatirà informació. Volem que el articulocarritos es vagi emplenant, però cada cop que el codi corre colem que el contenidor estigui buit per tal de posar només un curs en el carro.
    // Forma lenta
    // contenedorCarrito.innerHTML = '';       // Posem en blanc per netajar la taula
    while(contenedorCarrito.firstChild) {
        contenedorCarrito.removeChild(contenedorCarrito.firstChild)
    }
}

// Hem de tenir present dos conceptes importants, articulosCarrito és una array on guardem els objectes dels cursos i contenedorCarrito és la taula HTML on hi tenim la informació.
// Per tant, per actualitzar el carrito hem de borrar el contenidor i tornar-lo a crear amb l'arrazy que va guardant els cursos, sino en el carrito sortiria (A-AAB-AABABC).

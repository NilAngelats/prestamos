import * as UI from './interfaz.js';

export class ApiArtistTitle {
            constructor (artist, title) {
                this.artist = artist;
                this.title = title
            }
            applyApi () {
                const url = `https://api.lyrics.ovh/v1/${this.artist}/${this.title}`

                spinner()

                setTimeout(() => {
                    fetch (url)
                    .then(response => response.json())
                    .then(data => {
                        if(data.lyrics){
                            const {lyrics} = data;
                            UI.heading.innerHTML = `${this.artist}: ${this.title}`;
                            UI.result.textContent = lyrics;
                        } else {
                            console.log('hiho')
                            UI.messageDiv.textContent = 'The artist or the song does not exist, try again.';
                            UI.messageDiv.classList.add("error");
                        }
                    })
                    .catch(error => console.log(error))
                }, 2000);
            }
}

function spinner () {
    const spinner = document.createElement('div');
    spinner.classList.add('spinner');
    UI.result.appendChild(spinner);
}

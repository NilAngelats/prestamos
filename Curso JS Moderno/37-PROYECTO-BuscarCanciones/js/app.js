import * as UI from './interfaz.js';
import { ApiArtistTitle } from './api.js';


UI.form.addEventListener('submit', searchSong);

function searchSong (e) {
    e.preventDefault();
    const artist = document.querySelector("#artista").value;
    const title = document.querySelector("#cancion").value;

    if(artist === "" || title === "") {
        UI.messageDiv.textContent = 'Please fill in all required fields.'
        UI.messageDiv.classList.add('error');

        setTimeout(() => {
            UI.messageDiv.textContent = ''
            UI.messageDiv.classList.remove('error');
        }, 2000);
        
    } else {
        const apiQuery = new ApiArtistTitle (artist, title);
        apiQuery.applyApi()
    }   
}

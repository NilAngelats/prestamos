//Variables
const agregar = document.querySelector('#formulario');
const listaTweets = document.querySelector('#lista-tweets');
let tweets = [];


eventListeners();

//Eventlisteners
function eventListeners () {
    agregar.addEventListener('submit', agregarTweet);

    document.addEventListener('DOMContentLoaded', function () {
        tweets = JSON.parse(localStorage.getItem('tweets')) || []; // AIXO ES IMPORTANT aquesta es una funció que al carregar la pàgina carrega els tweets que han estat guardats previament, pero si per exemple no hi havia cap tweet previament, la variable tweets es guardarà com a nul·la. Això és un problema, ja que el forEach no pot iterar per a valors nulls llavors les funcions no faran el que toca. Per això hem de utilitzar el || i donar l'opció de que sigui una [] en cas de que no hi hagi cap tweet previament.
        crearHTML();
    });
}


function agregarTweet (e) {
    e.preventDefault();
    const tweet = document.querySelector('#tweet').value;
    if (tweet === '') {
        mostrarError ('El mensaje no puede ir vacio');
        
        return;
    }

    const tweetObj = {                        
        id: Date.now(),
        tweet
    };
    
    tweets = [...tweets, tweetObj];
    
    crearHTML();

    //Reiniciar el formulario
    agregar.reset();
};

function mostrarError (texto) {
    const errorMensaje = document.createElement('p');
    errorMensaje.textContent = texto;
    errorMensaje.classList.add('error');

    //On inserir
    const contenido = document.querySelector('#contenido');
    contenido.appendChild(errorMensaje);

    setTimeout(() => {
        errorMensaje.remove();
    }, 1000);
}

function crearHTML() {    
    limpieza();

    if (tweets.length > 0) {
        tweets.forEach( tweet => {
            const boton = document.createElement('a');
            boton.classList.add('borrar-tweet');
            boton.textContent = 'X';

            //Eliminar 
            boton.onclick = () => {
                borrarTweet(tweet.id)
            }

            const li = document.createElement('li');

            li.innerText = tweet.tweet;
            li.appendChild(boton);

            listaTweets.appendChild(li);
        });
    }
    guardarLocalStorage();
}

function limpieza () {
    while(listaTweets.firstChild) {
        listaTweets.removeChild(listaTweets.firstChild)
    }
}

function guardarLocalStorage () {
    localStorage.setItem('tweets',JSON.stringify(tweets));
}

function borrarTweet (id) {
    tweets = tweets.filter ( tweet => tweet.id !== id);
    crearHTML()
}
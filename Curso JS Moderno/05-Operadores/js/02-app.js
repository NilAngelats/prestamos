const numeroa = 5;
const numerob = "5";
const numeroc = 10;

// Comparadors

console.log(numeroa == numerob);
console.log(numeroa == numeroc);

// Comparador estricto

console.log(numeroa === numerob);
console.log(numeroa === numeroc);

// Comparador diferente

console.log(numeroa != numerob);
console.log(numeroa != numeroc);

// Comparador diferente estricto

console.log(numeroa !== numerob);
console.log(numeroa !== numeroc);

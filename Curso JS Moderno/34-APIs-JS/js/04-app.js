const fullscreen = document.querySelector("#abrir-pantalla-completa");
const exitFullscreen = document.querySelector("#salir-pantalla-completa");

fullscreen.addEventListener('click', fullscreenFunction);
exitFullscreen.addEventListener('click', exitFullscreenFunction);

function fullscreenFunction () {
    document.documentElement.requestFullscreen();
};

function exitFullscreenFunction () {
    document.exitFullscreen();
};
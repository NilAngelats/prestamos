const notificationBttn = document.querySelector("#notificar");
const popUp = document.querySelector("#verNotificacion");

notificationBttn.addEventListener('click', () => {
    Notification
        .requestPermission()
        .then(result => {
            console.log(`El resultado es ${result}`)
        })
})

popUp.addEventListener('click', () => {
    if(Notification.permission === 'granted') {
        const popUpJuan = new Notification('Esta es la notificación', {
            icon: 'img/ccj.png',
            body:'Codigo con Juan'
        });
        popUpJuan.onclick = function() {
            window.open('https://www.guerrastribales.es/')
        }
    }
    

})
//Variables
const formulario = document.querySelector("#agregar-gasto");
const listaGastos = document.querySelector("#gastos");


//Classes
class Presupuesto {
    constructor(presupuestoPrompt) {
        this.presupuesto = presupuestoPrompt;
        this.restante = presupuestoPrompt;
        this.gastos = [];
    }
    cargarGastoArray (gastoObj) {
        this.gastos = [...this.gastos, gastoObj];
    }
    actualitzarRestante () {
        const gastado = this.gastos.reduce((total, gasto) => total + gasto.cantidad,0);
        this.restante = this.presupuesto - gastado;
        document.querySelector("#restante").textContent = this.restante;
    }
    borrarGasto(id) {
        this.gastos = this.gastos.filter(gasto => gasto.id !== id)
        console.log(this.gastos)
    }
}

class UI {
    agregarPresupuestoYRestante (presupuestoObj) {
        const { presupuesto, restante} = presupuestoObj;
        document.querySelector("#total").textContent = presupuesto;
        document.querySelector("#restante").textContent = restante;
    }
    mensajeGasto (mensaje, tipo) {
        const div = document.createElement("div")
        div.classList.add("text-center", "alert");
        div.textContent = mensaje;
        const inserirMensajeDiv = document.querySelector("#inserirMensaje div");
        if (inserirMensajeDiv != null) {
            inserirMensajeDiv.remove()
        }
        if (tipo === "error") {            
            div.classList.add("alert-danger");
        } else {
            div.classList.add("alert-success");
        }
        document.querySelector("#inserirMensaje").appendChild(div);
        setTimeout(() => {
            div.remove()
        }, 2000);
    }
    limpieza () {
        while(listaGastos.firstChild) {
            listaGastos.removeChild(listaGastos.firstChild)
        }
    }
    cargarGastoHTML(gastos) {
        this.limpieza ()

        gastos.forEach(gasto => {
            const {nombreGasto, cantidad, id} = gasto;
            const gastoHTML = document.createElement("li");
            gastoHTML.className = "list-group-item d-flex justify-content-between align-items-center";
            gastoHTML.setAttribute("dat-id", id)
            gastoHTML.innerHTML = ` ${nombreGasto}<span class="badge badge-primary badge-pill"> ${cantidad} €</span>`;
            listaGastos.appendChild(gastoHTML);
            const button = document.createElement("button");
            button.classList.add("btn", "btn-danger", "borrar-gasto");
            button.textContent = "Borrar X"
            gastoHTML.appendChild(button);
            button.onclick = () => borrarHTML(id)
      })
    }
    restanteStyle(presupestoObj) {
        const {presupuesto, restante} = presupestoObj;
        const restanteDiv = document.querySelector(".restante");
        if ((presupuesto/4)>restante) {
            restanteDiv.classList.remove("alert-success", "alert-warning");
            restanteDiv.classList.add("alert-danger")
        } else if ((presupuesto / 2 ) > restante) {
            restanteDiv.classList.remove("alert-succes");
            restanteDiv.classList.add("alert-warning");
        } else {
            restanteDiv.classList.remove('alert-danger', 'alert-warning');
            restanteDiv.classList.add('alert-success');
        }
        
        if (restante <= 0) {
            this.mensajeGasto("Se ha excedido el presupuesto", "error")
            formulario.querySelector('button[type="submit"]').disabled = true;
        }
    }
    
}

const ui = new UI;
let presupuestoEntrada;

//Event Listeners
cargarEventListeners();
function cargarEventListeners() {
    document.addEventListener("DOMContentLoaded", preguntarPresupesto);
    formulario.addEventListener("submit", agragarPresupuesto)
}


//Funcions
function preguntarPresupesto () {
    const presupestoUser = Number(prompt("¿Cual es un tu presupuesto"));
    if (presupestoUser <= 0 || isNaN(presupestoUser) || presupestoUser === "" || presupestoUser === null) {
        window.location.reload()
    }
    presupuestoEntrada = new Presupuesto(presupestoUser);
    ui.agregarPresupuestoYRestante(presupuestoEntrada)
}

function agragarPresupuesto (e) {
    e.preventDefault()
    const nombreGasto = document.querySelector("#gasto").value;
    const cantidad = Number(document.querySelector("#cantidad").value);
    if (nombreGasto === "" || cantidad ==="") {
        ui.mensajeGasto("Todos los campos son obligatorios", "error");
        return;
    } else if (isNaN(cantidad) || cantidad <= 0) {
        ui.mensajeGasto("Error en el campo cantidad", "error");
        return;
    } else if (Number(nombreGasto) || nombreGasto === "0") {
        ui.mensajeGasto("Error en el campo nombre", "error");
        return;
    }
    ui.mensajeGasto("El gasto se ha guardado correctamente", "success");
    const gasto = {nombreGasto, cantidad, id: Date.now()};
    presupuestoEntrada.cargarGastoArray(gasto);
    const {gastos} = presupuestoEntrada;
    ui.cargarGastoHTML(gastos)
    formulario.reset()
    presupuestoEntrada.actualitzarRestante();
    ui.restanteStyle(presupuestoEntrada);
    
}

function borrarHTML (id) {
    presupuestoEntrada.borrarGasto(id)
    presupuestoEntrada.actualitzarRestante ();
    const {gastos} = presupuestoEntrada
    ui.cargarGastoHTML(gastos);
    ui.restanteStyle(presupuestoEntrada)

}
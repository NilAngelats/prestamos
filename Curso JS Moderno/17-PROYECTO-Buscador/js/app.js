// Variables
const marca = document.querySelector("#marca");
const year = document.querySelector("#year");
const min = document.querySelector("#minimo");
const max = document.querySelector("#maximo");
const puertas = document.querySelector("#puertas");
const transmision = document.querySelector("#transmision");
const color = document.querySelector("#color");
const resultado = document.querySelector("#resultado");
const maxYear = new Date().getFullYear();
const minYear = maxYear - 12;

// No posem els anys en el HTML pq volem que s'actualitzi amb el canvi d'any, per aixó utilitzem el mètode getfullyear().

// Creem un objecte buit, que serà l'objecte de busqueda.
const datosBusqueda = {
    marca: "",
    year: "",
    min: "",
    max: "",
    puertas: "",
    transmision: "",
    color: "",
}

// Eventlisteners
document.addEventListener('DOMContentLoaded', () => { 
    llenarSelect();
})

marca.addEventListener('change', e => {
    datosBusqueda.marca = e.target.value;
    filtrarAuto();
});
year.addEventListener('change', e => {
    datosBusqueda.year = e.target.value;
    filtrarAuto();
});
min.addEventListener('change', e => {
    datosBusqueda.min = e.target.value;
    filtrarAuto();
});
max.addEventListener('change', e => {
    datosBusqueda.max = e.target.value;
    filtrarAuto();
});
puertas.addEventListener('change', e => {
    datosBusqueda.puertas = e.target.value;
    filtrarAuto();
});
transmision.addEventListener('change', e => {
    datosBusqueda.transmision = e.target.value;
    filtrarAuto();
});
color.addEventListener('change', e => {
    datosBusqueda.color = e.target.value;
    filtrarAuto(); 
});

 // Fem una funció que cridi a unes funcions, ja que quan carregui la pàgina volem que carregui més d'una funció. 
function mostrarResultados (autos) {
    limpieza ();
    autos.forEach( auto => {
        const {marca, modelo, year, precio, puertas, color, transmision} = auto;
        const autoHTML = document.createElement('p');
        autoHTML.innerHTML = `
            ${marca}: ${modelo} Año: ${year} <br>
            puertas: ${puertas} precio: ${precio} <br>
            color: ${color} transmisión: ${transmision}
        `
        resultado.appendChild(autoHTML);
    })    
}

function limpieza () {
    while(resultado.firstChild) {
        resultado.removeChild(resultado.firstChild);
    }
}

function llenarSelect () {
    for ( i = maxYear; i > minYear; i--) {
        const option = document.createElement('option');
        option.value = i;
        option.innerHTML = i;
        year.appendChild(option);
    }
}

// Funció de filtratge
function filtrarAuto () {
    const resultado = autos.filter(filtrarMarca).filter(filtrarYear).filter(filtrarMin).filter(filtrarMax).filter(filtrarPuerts).filter(filtrarTransmision).filter(filtrarColor);
    mostrarResultados(resultado);
    if (resultado.length === 19) {
        limpieza()
    } else if (resultado.length === 0) {
        noResultado();
    }
}

function noResultado() {
    const noResultado = document.createElement('div');
        noResultado.classList.add('alerta', 'error');
        noResultado.textContent = 'No se han encontrado coches con estas características';
        resultado.appendChild(noResultado);
}

function filtrarMarca (auto) {
    const {marca} = datosBusqueda;
    if (marca) {
        return auto.marca === datosBusqueda.marca;
    }
    return auto;
}
function filtrarYear (auto) {
    const {year} = datosBusqueda;
    if (year) {
        return auto.year === parseInt(datosBusqueda.year);
    }
    return auto;
}
function filtrarMin (auto) {
    const {min} = datosBusqueda;
    if (min) {
        return auto.precio >= parseInt(datosBusqueda.min);
    }
    return auto;
}
function filtrarMax (auto) {
    const {max} = datosBusqueda;
    if (max) {
        return auto.precio <= parseInt(datosBusqueda.max);
    }
    return auto;
}
function filtrarPuerts (auto) {
    const {puertas} = datosBusqueda;
    if (puertas) {
        return auto.puertas === parseInt(datosBusqueda.puertas);
    }
    return auto;
}
function filtrarTransmision (auto) {
    const {transmision} = datosBusqueda;
    if (transmision) {
        return auto.transmision === datosBusqueda.transmision;
    }
    return auto;
}
function filtrarColor (auto) {
    const {color} = datosBusqueda;
    if (color) {
        return auto.color === datosBusqueda.color;
    }
    return auto;
}



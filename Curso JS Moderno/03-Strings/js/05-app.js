// Replace
console.log(producto);
console.log(producto.replace("Estem", "Anem"));

// Slice
console.log(producto.slice(0,5));
console.log(producto.slice(6));
console.log(producto.slice(2,1));

// Substring
console.log(producto.substring(0,5));
console.log(producto.substring(6));
console.log(producto.substring(2,1));

// charAt
console.log(producto.charAt());
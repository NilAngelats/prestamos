const amics = {
    mane: "crac",
    piqueras:"pesat",
    prats: "jefe",
    purta: "solidari",
    marti: "diferent",
};

const amigues = {
    fayoliki: "top",
    carla: "culazo",
    fiona: "calenta",
    flores: "creu",
};

const relacio = Object.assign (amics,amigues);


amics.definicio = function () {
    console.log(`En mane és un ${this.mane}`)
};

relacio.definicio();

relacio.estabona = function(){
    console.log(`La carla té un ${this.carla}`)
}

relacio.estabona();

relacio.calenta = function(){
    console.log(`La Fiona és una ${this.fiona}`)
}

relacio.calenta();
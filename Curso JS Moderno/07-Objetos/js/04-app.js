const amics = {
    mane: "crac",
    piqueras: "pesat",
    prats: "jefe",
    uri: "divertit",
    marti: "diferent",
    purta: "solidari",
    noies: {
        fiona: "calenta",
        carla: "culazo",
        flores: "creu",
        fayoliki: "top",
        extra: {
            mascota: {
                gos: "Dana, Rita, Conri",
                gat: "Nit, Tura, Jalis",
            },
        },
    },
    pares: {
        mare: "pare",
    } 
};
console.log(amics);
const {mane, purta} = amics;
console.log(mane);
console.log(purta);
const {fiona} = amics.noies;
console.log(fiona);
 const {gat} = amics.noies.extra.mascota;
 console.log(gat);
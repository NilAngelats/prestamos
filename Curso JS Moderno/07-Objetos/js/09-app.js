const amics = {
    mane: "crac",
    piqueras:"pesat",
    prats: "jefe",
    purta: "solidari",
    marti: "diferent",
};

const amigues = {
    fayoliki: "top",
    carla: "culazo",
    fiona: "calenta",
    flores: "creu",
};

// Utilizamos el método Object.assign para unir los dos objetos
const unio = Object.assign(amics, amigues);
console.log(unio);

// Utilizamos ... para copiar y unir.

const junts = {...amics,...amigues};
console.log(junts);

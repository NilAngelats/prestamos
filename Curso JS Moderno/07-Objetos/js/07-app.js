const amics = {
    mane: "crac",
    piqueras: "pesat",
    prats: "jefe",
    uri: "divertit",
    marti: "diferent",
    purta: "solidari",
    noies: {
        fiona: "calenta",
        carla: "culazo",
        flores: "creu",
        fayoliki: "top",
        extra: {
            mascota: {
                gos: "Dana, Rita, Conri",
                gat: "Nit, Tura, Jalis",
            },
        },
    },
    pares: {
        mare: "pare",
    } 
};

// Denegar el permiso a modificar, inserir o eliminar llaves y valores en el objeto

Object.freeze(amics);

delete amics.piqueras;

console.log(amics);
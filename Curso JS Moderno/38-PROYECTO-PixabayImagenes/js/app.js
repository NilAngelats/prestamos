const form = document.querySelector("#formulario");
const result = document.querySelector("#resultado");
const pages = document.querySelector("#paginacion");
const numberOfPages = 40;
let totalPages;
let iterator;
let pageValue = 1;

document.addEventListener('DOMContentLoaded', () => {
    form.addEventListener('submit', searchImages);
})

function searchImages (e) {
    e.preventDefault();
    const formInput = document.querySelector("#termino").value;
    if (formInput === "") {
        showUpTheAlert('Please fill in the field');
        return;
    };
    requestApi(formInput)
};

function showUpTheAlert (message) {
    const notRepeat = document.querySelector(".not-repeat");
    if(!notRepeat) {
        const alert = document.createElement('p');
        alert.classList.add('bg-red-100', "border-red-400", "text-red-700", "px-4", "py-3", "rounded",  "max-w-lg", "mx-auto", "mt-6", "text-center", 'not-repeat');
        alert.textContent = message;
        form.appendChild(alert);

        setTimeout(() => {
            alert.remove()
        }, 2000);
    }
};

function requestApi () {
    const input = document.querySelector("#termino").value;
    const key = '20470958-10c938bfca5fdec7b49128af3';
    const url = `https://pixabay.com/api/?key=${key}&q=${input}&per_page=${numberOfPages}&page=${pageValue}`;
    fetch(url)
        .then(response => response.json())
        .then(data => {
            totalPages = countPages(data.totalHits)
            showFromApi(data.hits)
        })
};

function countPages (pages) {
    return parseInt(Math.ceil(pages/numberOfPages));
};

function showFromApi (dataHits) {
    while(result.firstChild) {
        result.removeChild(result.firstChild);
    }
    dataHits.forEach (image => {
        const {likes, previewURL, views, largeImageURL} = image;
        result.innerHTML += `
            <div class="w-1/2 md:w-1/3 lg:w-1/4 p-3 mb-4" >
                <div class= "bg-white">
                    <img class="w-full" src="${previewURL}">
                    <div class= "p-4">
                        <p class="font-bold"> ${likes}  <span class="font-light">likes</span></p>
                        <p class="font-bold"> ${views} <span class="font-light">views</span></p>
                        <a
                            class="bg-blue-800 w-full p-1 block mt-5 rounded text-center font-bold uppercase hover:bg-blue-500 text-white"
                            href="${largeImageURL}" target="_blank" rel="noopener noreferrer"
                        > Show Image </a>
                    </div>
                </div>
            </div>
        `
    })

    while(pages.firstChild) {
        pages.removeChild(pages.firstChild);
    }
    
    startGenerator ();    
};

function *generator (totalPageOverAll) {
    for(let i = 1; i <= totalPageOverAll; i++) {
        yield i;
    }
};

function startGenerator () {
    iterator = generator(totalPages);

    while (true) {
        const {value, done} = iterator.next();
        if(done) {
            return 
        };
        const button = document.createElement("a");
        button.href ="#"
        button.dataset.page = value;
        button.textContent = value;
        button.classList.add('siguiente', 'bg-yellow-400', 'px-4', 'py-1', 'mr-2', 'mb-4', 'font-bold', 'rounded');
        button.onclick = () => {
            pageValue = value;
            requestApi()
        }
        pages.appendChild(button);

    }
};
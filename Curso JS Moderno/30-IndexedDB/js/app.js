let DB;

document.addEventListener('DOMContentLoaded', () => {
    crmDB();

    setTimeout(() => {
        crearCliente()
    }, 2000);
})

function crmDB () {
    let crmbd = window.indexedDB.open('crm', 1);

    crmbd.onerror = () => {
        console.log('Error');
    }

    crmbd.onsuccess = () => {
        console.log('Success');
        DB = crmbd.result;
    }

    crmbd.onupgradeneeded = (e) => {
        let db = e.target.result;
        let objectStore = db.createObjectStore('crm', { keyPath: 'crm',  autoIncrement: true } );

        objectStore.createIndex ('nombre', 'nombre', { unique: false } );
        objectStore.createIndex ('email', 'email', { unique: true } );
        objectStore.createIndex ('telefono', 'telefono', { unique: true } );
    }
}

function crearCliente () {
    let transaction = DB.transaction(['crm'], 'readwrite');

    transaction.oncomplete =  () => {
        console.log('Transaction successed');
        
    }

    transaction.onerror =  () => {
        console.log('Transaction failed');
        
    }
    
    let objectStore = transaction.objectStore('crm')

    const cliente = {
        nombre: 'Nil',
        email: 'nilangelatsregincos@gmail.com',
        telefono: 664294169
    }
    let peticion = objectStore.add(cliente)

}
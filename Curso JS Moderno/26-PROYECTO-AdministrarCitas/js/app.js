//Variables
const mascotaInput = document.querySelector("#mascota");
const propietarioInput = document.querySelector("#propietario");
const telefonoInput = document.querySelector("#telefono");
const fechaInput = document.querySelector("#fecha");
const horaInput = document.querySelector("#hora");
const sintomasInput = document.querySelector("#sintomas");

const formulario = document.querySelector("#nueva-cita");
const listaCitas = document.querySelector("#citas");

let editant; 

//Event Listener
cargarEventListeners();
function cargarEventListeners () {
    mascotaInput.addEventListener("input", agrgarInfoObjecte);
    propietarioInput.addEventListener("input", agrgarInfoObjecte);
    telefonoInput.addEventListener("input", agrgarInfoObjecte);
    fechaInput.addEventListener("input", agrgarInfoObjecte);
    horaInput.addEventListener("input", agrgarInfoObjecte);
    sintomasInput.addEventListener("input", agrgarInfoObjecte);
    formulario.addEventListener("submit", agregarCita);
}

//Classes
class Citas {
    constructor () {
        this.citas = [];
    }
    agregarObjecteArray(citaObjecte) {
        this.citas = [...this.citas, citaObjecte];
    }
    eliminarCitaHTML (id) {
        this.citas = this.citas.filter(cita => cita.id !== id)
    }
    editarCitasHTML (citasActualitzada) {
        this.citas = this.citas.map(cita => cita.id === citasActualitzada.id ? citasActualitzada : cita)
    }

}

class UI {
    missatge (misatge, tipus) {
        const div = document.createElement("div");
        div.classList.add("text-center", "alert", "col-12");
        div.textContent = misatge;
        const inserirMisatgeDiv = document.querySelector("#inserir-mensaje div");
        if (inserirMisatgeDiv !== null) {
            inserirMisatgeDiv.remove();
        }
        if (tipus === "error") {
            div.classList.add("alert-danger");
        } else {
            div.classList.add("alert-success");
        }
        const inserirMisatge = document.querySelector("#inserir-mensaje");
        inserirMisatge.appendChild(div);
        setTimeout(() => {
            div.remove()
        }, 2000);
    }
    agregarCitaHTML(citaEntrada) {
        const citasArray = citaEntrada.citas;

        this.netejaHTML ()

        citasArray.forEach(cita => {
            const {mascota, propietario, telefono, fecha, hora, sintomas, id} = cita;

            const divCitaHTML = document.createElement("div");
            divCitaHTML.classList.add("cita", "p-3")
            divCitaHTML.setAttribute("data-id", id);            

            const mascotaTitol = document.createElement("h2");
            mascotaTitol.classList.add("card-title", "font-weight-bolder");
            mascotaTitol.innerHTML = `${mascota}`;

            const propietarioParagraf = document.createElement("p");
            propietarioParagraf.innerHTML = `<span class="font-weight-bolder">Propietario: </span>${propietario}`;

            const telefonoParagraf = document.createElement("p");
            telefonoParagraf.innerHTML = `<span class="font-weight-bolder">Teléfono: </span>${telefono}`;

            const fechaParagraf = document.createElement("p");
            fechaParagraf.innerHTML = `<span class="font-weight-bolder">Fecha: </span>${fecha}`;

            const horaParagraf = document.createElement("p");
            horaParagraf.innerHTML = `<span class="font-weight-bolder">Hora: </span>${hora}`;

            const sintomasioParagraf = document.createElement("p");
            sintomasioParagraf.innerHTML = `<span class="font-weight-bolder">Síntomas: </span>${sintomas}`;
            
            const deleteButton = document.createElement("button");
            deleteButton.classList.add("btn", "btn-danger", "mr-2");
            deleteButton.innerHTML = 'Eliminar <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" /></svg>'
            deleteButton.onclick = () => {
                eliminarCita(id);
            }
            
            const editButton = document.createElement("button");
            editButton.classList.add("btn", "btn-info");
            editButton.innerHTML = 'Editar <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" /></svg>';
            editButton.onclick = () => {
                editarCita(cita);
            }

            divCitaHTML.appendChild(mascotaTitol);
            divCitaHTML.appendChild(propietarioParagraf);
            divCitaHTML.appendChild(telefonoParagraf);
            divCitaHTML.appendChild(fechaParagraf);
            divCitaHTML.appendChild(horaParagraf);
            divCitaHTML.appendChild(sintomasioParagraf);
            divCitaHTML.appendChild(deleteButton);
            divCitaHTML.appendChild(editButton);

            listaCitas.appendChild(divCitaHTML);            
        })
    }
    netejaHTML () {
        while(listaCitas.firstChild) {
            listaCitas.removeChild(listaCitas.firstChild)
        }
    }
    refrescarObjecte () {
        citaObjecte.mascota = "";
        citaObjecte.propietario = "";
        citaObjecte.telefono = "";
        citaObjecte.fecha = "";
        citaObjecte.hora = "";
        citaObjecte.sintomas = "";
    }
}


const citaEntrada = new Citas();
const ui = new UI();

//Objecte
const citaObjecte = {
    mascota: "",
    propietario: "",
    telefono: "",
    fecha: "",
    hora: "",
    sintomas: "",
}

//Funcions
function agregarCita (e) {
    e.preventDefault();
    const {mascota, propietario, telefono, fecha, hora, sintomas} = citaObjecte;
    if (mascota === "" || propietario === "" || telefono === "" || fecha === "" || hora === "" || sintomas === "") {
        ui.missatge("Tots els camps són obligatoris", "error");
        return;
    }
    if (editant) {
        ui.missatge("La cita s'ha editat correctament", "success");
        document.querySelector('button[type="submit"]').textContent = "Guardar cambios";
        citaEntrada.editarCitasHTML ({...citaObjecte})
        editant = false;
    }else {
        citaObjecte.id = Date.now();
        citaEntrada.agregarObjecteArray({...citaObjecte});
        ui.missatge("La cita s'ha afegit correctament", "success");
    }
    ui.agregarCitaHTML(citaEntrada);
    ui.refrescarObjecte ()
    formulario.reset()
}

function agrgarInfoObjecte (e) {
    citaObjecte[e.target.name] = e.target.value;
}

function eliminarCita (id) {
    citaEntrada.eliminarCitaHTML(id);
    ui.agregarCitaHTML(citaEntrada);
    ui.missatge("La cita s'ha esborrat correctament", "success");
}

function editarCita (cita) {
    const {mascota, propietario, telefono, fecha, hora, sintomas, id} = cita;

    citaObjecte.mascota = mascota;
    citaObjecte.propietario = propietario;
    citaObjecte.telefono = telefono;
    citaObjecte.fecha = fecha;
    citaObjecte.hora = hora;
    citaObjecte.sintomas = sintomas;
    citaObjecte.id = id;

    mascotaInput.value = mascota;
    propietarioInput.value = propietario;
    telefonoInput.value = telefono;
    fechaInput.value = fecha;
    horaInput.value = hora;
    sintomasInput.value = sintomas;
    
    document.querySelector('button[type="submit"]').textContent = "Guardar cambios";

    editant = true;
}
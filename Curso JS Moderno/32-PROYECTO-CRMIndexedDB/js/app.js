(function () {

    let DB;

    window.onload= () => {
        createDB()
    }

    function createDB () {
        const dataBase = window.indexedDB.open ('crm', 1);
        dataBase.onerror = () => {
            console.log('error')
        }

        dataBase.onsuccess = () => {
            DB = dataBase.result;
        }

        dataBase.onupgradeneeded = (e) => {
            const db = e.target.result;
            const objectStore = db.createObjectStore('crm', { keyPath: 'id',  autoIncrement: true } );

            objectStore.createIndex('nombre', 'nombre', { unique: false } );
            objectStore.createIndex('email', 'email', { unique: true } );
            objectStore.createIndex('telefono', 'telefono', { unique: false } );
            objectStore.createIndex('empresa', 'empresa', { unique: false } );
            objectStore.createIndex('id', 'id', { unique: true } );

            console.log('The database has been created')
        }
    }

})()
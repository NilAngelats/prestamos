(function() {
    let DB;
    const formulario = document.querySelector("#formulario");
    
    document.addEventListener('DOMContentLoaded', () => {

        formulario.addEventListener('submit', authentication);
        conncetionDB()

    })

    function conncetionDB () {
        const connection = window.indexedDB.open("crm", 1);

        connection.onerror = () => {
            printAlert('The connection to the database has failed', "error")
        }

        connection.onsuccess = () => {
            DB = connection.result
            console.log('Success')
        }
    }

    function authentication (e) {
        e.preventDefault();

        const nombre = document.querySelector("#nombre").value;
        const email = document.querySelector("#email").value;
        const telefono = document.querySelector("#telefono").value;
        const empresa = document.querySelector("#empresa").value;

        if(nombre === "" || email === "" || telefono === "" || empresa === "") {
            printAlert('Please, fill all the required fields', "error")
        }

        const customer = {
            nombre: nombre, 
            email: email, 
            telefono: telefono, 
            empresa: empresa, 
            id: Date.now()
        };
        createCustomer(customer);
        // printCustomer ();
    }

    // function printCustomer () {
    //     const objectStore = DB.transaction('crm').objectStore('crm');
    //     objectStore.openCursor().onsuccess = function (e) {
    //         console.log(e.target.result)
    //     }
    // }

    function createCustomer (customer) {
        const transaction = DB.transaction(['crm'], 'readwrite');
        // const objectStore = transaction.objectStore('crm');
        // objectStore.add(customer)
        // printAlert('A new customer has been added', "success");

        // setTimeout(() => {
        //     window.location.href = "index.html"
        // }, 2500);

    }

    function printAlert (message, type) {
        const alerta = document.querySelector(".alerta");
        if(!alerta) {
            const div = document.createElement("div");
            div.classList.add("px-4", "py-3", "rounded",  "max-w-lg", "mx-auto", "mt-6", "text-center", "alerta");
            div.textContent = message;
            if(type === "error") {
                div.classList.add('bg-red-100', 'border-red-400', "text-red-700")
            } else {
                div.classList.add('bg-green-100', "border-green-400", "text-green-700");
            }

            setTimeout(() => {
                div.remove()
            }, 2500);

            formulario.appendChild(div)
        }
        
    }

})();





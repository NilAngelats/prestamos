const bttnTXT = document.querySelector("#cargarTxt");

bttnTXT.addEventListener('click', textLoad);

function textLoad () {
    fetch('data/datos.txt')
        .then(resultado => resultado.text())
        .then(datos => console.log(datos))
        .catch(error => console.log(error))
}
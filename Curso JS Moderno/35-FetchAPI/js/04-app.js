const bttnAPI = document.querySelector("#cargarAPI");

bttnAPI.addEventListener('click', apiLoad);

function apiLoad () {
    fetch('https://picsum.photos/list')
        .then(response => response.json())
        .then(data => showData(data))
};

function showData(data) {

    const container = document.querySelector(".contenido")

    let html = ""

    data.forEach(person => {
        const {id, author, author_url} = person
        
        html += `
            <p> id: ${id}</p>
            <p> author: ${author}</p>
            <a href="${author_url}">Ver</a>
        `

    });

    container.innerHTML = html;
    
}
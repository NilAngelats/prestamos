document.addEventListener('DOMContentLoaded', jsonArrayLoad)
const bttnJsonArray = document.querySelector("#cargarJSONArray");

bttnJsonArray.addEventListener("click", jsonArrayLoad);

function jsonArrayLoad () {
    fetch('data/empleados.json')
        .then(response => response.json())
        .then(data => showHtml(data))
};

function showHtml (employers) {
    const container = document.querySelector(".contenido");

    let html= "";

    employers.forEach(employer => {

        const {id, nombre, empresa, trabajo} = employer;

        html += `
            <p> Id: ${id}</p>
            <p> nombre: ${nombre}</p>
            <p> empresa: ${empresa}</p>
            <p> trabajo: ${trabajo}</p>
        `
    })
    container.innerHTML = html;

}
// Scroll
// Window és tota la pàgina web, de fet dins de window hi trobem
// document. Llavors a partir de window podem treballar el scroll.
// Si hi ha doble class mira't bé quina és la distintiva, ja que en aquest
// cas premium és la distintiva. Pq per exemple hi ha diferents contenedor-cards
window.addEventListener('scroll', () => {
    const premium = document.querySelector('.premium');
    const ubicacion = premium.getBoundingClientRect();
    //console.log(ubicacion)
    if (ubicacion.top<457) {
        console.log('Has arribat nanu')
    } else {
        console.log('Encara no')
    }
})

// Com podem veure utilitzem un mètode .getBoundingClientRect() en premium
// per tal de trobar la seva ubicació i d'aquesta manera poder agregar funcionalitat
// en aquella part de window quan hi arribem. Ens hem de fixar en bottom o top, que ens
// donarà la distància que hi ha respecte la part seleccionada.
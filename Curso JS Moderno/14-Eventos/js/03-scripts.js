// Parlarem del formulari i del buscador

const buscar = document.querySelector('#formulario');

// Podem veure que no es va a buscar el id del botó.
// Sino l'id de form, on hi ha una action i method!

buscar.addEventListener('submit', (e) => {
    e.preventDefault();
    console.log('Passa!!');
    console.log(e.target.action);       // file:///C:/buscador
    console.log(e.target.method);       // post
});

// Posem .preventDefault () per evitar que form faci l'acció de 
// POST. Ja que primer de tot volem assegurar que tot funcioni, un cop
// ja ho hem assegurat podem demanar una API amb resultats reals per tal de
// confirmar que tot funciona correctament. O bé, per fer una validació
// de les dades abans d'enviar-les.

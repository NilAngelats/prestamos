const container = document.querySelector(".container");
const result = document.querySelector("#resultado");
const form = document.querySelector("#formulario");

window.addEventListener("load", () => {
    form.addEventListener("submit", weatherSearch)
});


function weatherSearch (e) {
    e.preventDefault();
    const city = document.querySelector("#ciudad").value;
    const country = document.querySelector("#pais").value;
    
    if(city === "" || country === "") {
        showUpMessage('Please fill all the required fields.');
        return
    }
    showUpCityCountry(city, country)
};

function showUpMessage (message) {
    const alerta = document.querySelector('.alerta-error-created');
    
    if(!alerta) {
        const errorMessage = document.createElement('div');
        errorMessage.classList.add('alerta-error-created', 'bg-red-100', "border-red-400", "text-red-700", "px-4", "py-3", "rounded", "relative", "max-w-md", "mx-auto", "mt-6", "text-center");
        container.appendChild(errorMessage);
        errorMessage.innerHTML = message;
        setTimeout(() => {
            errorMessage.remove()
        }, 2000);
    }
};

function showUpCityCountry (city, country) {
    const apiId = 'f40685dabb35b8a8f1829af7c8d55164'
    const url = `https://api.openweathermap.org/data/2.5/weather?q=${city},${country}&appid=${apiId}`

    spinner ()

    setTimeout(() => {
        fetch(url)
        .then(response => response.json())
        .then(data => {

            cleanHTML ();

            if(data.cod === '404') {
                showUpMessage('The city does not exist.');
                return;
            }
            showUpTemperature(data);
            
        })
    }, 1500);
};

function showUpTemperature(data) {
    const {name, main: {temp, temp_max, temp_min} } = data;
    const centi = kelvinToCent(temp);
    const max = kelvinToCent(temp_max); 
    const min = kelvinToCent(temp_min); 

    const nameCity = document.createElement('p');
    nameCity.classList.add('font-bold', 'text-2xl')
    nameCity.innerHTML = `${name}`;


    const currentTemp = document.createElement('p');
    currentTemp.classList.add('font-bold', 'text-6xl')
    currentTemp.innerHTML = `${centi} &#8451;`;

    const currentTempMax = document.createElement('p');
    currentTempMax.classList.add('text-xl')
    currentTempMax.innerHTML = `Max: ${max} &#8451;`;

    const currentTempMin = document.createElement('p');
    currentTempMin.classList.add('text-xl')
    currentTempMin.innerHTML = `Min: ${min} &#8451;`;

    const div = document.createElement('div');
    div.classList.add('text-center', 'text-white');
    div.appendChild(nameCity);
    div.appendChild(currentTemp);
    div.appendChild(currentTempMax);
    div.appendChild(currentTempMin);
    result.appendChild(div)
}

const kelvinToCent = grados => parseInt(grados - 273.15);

function spinner () {

    cleanHTML ();

    const spinnerDiv = document.createElement('div');
    spinnerDiv.classList.add('sk-cube-grid');
    spinnerDiv.innerHTML = `
        <div class="sk-cube sk-cube1"></div>
        <div class="sk-cube sk-cube2"></div>
        <div class="sk-cube sk-cube3"></div>
        <div class="sk-cube sk-cube4"></div>
        <div class="sk-cube sk-cube5"></div>
        <div class="sk-cube sk-cube6"></div>
        <div class="sk-cube sk-cube7"></div>
        <div class="sk-cube sk-cube8"></div>
        <div class="sk-cube sk-cube9"></div>
    `
    result.appendChild(spinnerDiv)
}

function cleanHTML () {
    while(result.firstChild) {
        result.removeChild(result.firstChild)
    }
}
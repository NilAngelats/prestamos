REFERENCE 

(function () {
  const button = document.querySelectorAll('.rsform-button-next, .rsform-button-prev');

  button.forEach(el => {
    el.closest('.rsform-block').classList.add('rsform-weiter');
  });
})(); //RSform next/prev button check

// ###################################

(function () {
  const table = document.querySelectorAll('.change-to-table');

  table.forEach(el => {
    el.closest('.formRow').classList.add('rsform-change-to-table');
  });
})(); //RSform Table check

// ###################################

(function () {
  const table = document.querySelectorAll('.rsform-block-fragenkatalog-c-7-1-line-2-r');

  table.forEach(el => {
    el.closest('.formRow').classList.add('rsform-custom-table');
  });
})(); //RSform Table check 2

// ###################################

(function () {
  const nameArray = ['F__3-1', 'F__3-2', 'F__4-2', 'F__4-3'];
  const maxNr = 8;
  let tableList = [];

  nameArray.forEach(el => {
    for (let i = 1; i <= maxNr; i++) {
      let tableListWoah = document.getElementById(`fragenkatalog-${el}__pers-${i}__name`)
      
      if (tableListWoah) {
        tableList.push(tableListWoah);
      }
    }
  });

  let checkTableName = (el, finalElement) => {
    if (el.value !== "") {
      finalElement.style.display = 'flex';
    } else {
      finalElement.style.display = 'none';
    }
  };

  tableList.forEach(name => {
    const finalElement = name.closest('.formRow').nextElementSibling;
    finalElement.style.display = 'none';

    name.oninput = checkTableName.bind(this, name, finalElement);
  });
})(); //RSform Table Enable names

// ###################################

var slider = document.querySelector('#myRange');
var output = document.querySelector("#value-range");
var output2 = document.querySelector("#people-range");
let rangeValue = document.querySelector('#final-price');
const users = document.querySelector("#user");
const userPriceCounter = document.querySelector("#user-price");
const boxesContainer = document.querySelector('.price-boxes__container');
let box1Price = document.querySelector("#price-box-1");
let box2Price = document.querySelector("#price-box-2");
let box3Price = document.querySelector("#price-box-3");
let box4Price = document.querySelector("#price-box-4");
let box1 = document.querySelector("#box-1");
let box2 = document.querySelector("#box-2");
let box3 = document.querySelector("#box-3");
let box4 = document.querySelector("#box-4");
const buybutton = document.querySelector("#pay-button");
let total1 = 0;
let total2 = 0;
let total3 = 0;
let total4 = 0;
var style = document.querySelector('[data="test"]');
var style2 = document.querySelector('[data="test2"]');

// Checkbox by ID

boxesContainer.onchange = function(e) {
    if (e.target.classList.contains('checkbox')){
        const id = e.target.getAttribute('id');
        check(slider.value)        
    }
}

// Slider value, user and price per user value. Checkboxes values accordin to the slider. Bubbles size.
output.classList.add("show");
output2.classList.add("show");

slider.oninput = function () {
    let value = slider.value;

    userPrice(value)    
    output.style.left = (value/2.88);
    
    
    rangeSlide2(value)
    setDataSize(value)
    
    output2.innerHTML = `${value}`;
    output2.style.left = (value/2.88);
    
    slider.style.setProperty("--thumb-rotate", `${value}deg`)
 
    box1Price.innerHTML = (addon1[value -25]).toFixed(2).toString().replace(".", ",");
    box2Price.innerHTML = (addon2[value -25]).toFixed(2).toString().replace(".", ",");
    box3Price.innerHTML = (addon3[value -25]).toFixed(2).toString().replace(".", ",");
    box4Price.innerHTML = (addon4[value -25]).toFixed(2).toString().replace(".", ",");
}

function userPrice (value) {
    if (value == 25) {
        output.innerHTML = '6 €'
    } else {
        output.textContent = `${prices[value-25] - prices[value-26]} €`;
    }
}

// Current - Prev slider movement.

var rangeSlide2 = (function() {
    let previousValue = null;
    return function(value) {
      if (previousValue !== null && previousValue < value) {
        check(value)
        previousValue = value;
      } else if (previousValue !== null && previousValue > value) {
        check(value)
        previousValue = value;
      } else if (previousValue === null) {
        rangeValue.innerHTML = prices[value -25];
        previousValue = value;
      }
    };
  })();


// Add to the counter the value of the checked boxes.

function check (value) {
    if (box1.checked === true && box2.checked === true && box3.checked === true && box4.checked === true) {
        rangeValue.innerHTML = (prices[value -25] + addon1[value -25] + addon2[value -25] + addon3[value -25] + addon4[value -25]).toFixed(2).toString().replace(".", ","); 
    } else if (box1.checked === true && box2.checked === true && box3.checked === true){
       rangeValue.innerHTML = (prices[value -25] + addon1[value -25] + addon2[value -25] + addon3[value -25]).toFixed(2).toString().replace(".", ",");
    } else if (box1.checked === true && box2.checked === true && box4.checked === true){
        rangeValue.innerHTML = (prices[value -25] + addon1[value -25] + addon2[value -25] + addon4[value -25]).toFixed(2).toString().replace(".", ",");
    } else if (box2.checked === true && box3.checked === true && box4.checked === true){
        rangeValue.innerHTML = (prices[value -25] + addon2[value -25] + addon3[value -25] + addon4[value -25]).toFixed(2).toString().replace(".", ",");
    } else if (box1.checked === true && box2.checked === true && box3.checked === true){
        rangeValue.innerHTML = (prices[value -25] + addon1[value -25] + addon3[value -25] + addon4[value -25]).toFixed(2).toString().replace(".", ",");
    }else if (box1.checked === true && box2.checked === true){
        rangeValue.innerHTML = (prices[value -25] + addon1[value -25] + addon2[value -25]).toFixed(2).toString().replace(".", ",");
    }else if (box1.checked === true && box3.checked === true){
        rangeValue.innerHTML = (prices[value -25] + addon1[value -25] + addon3[value -25]).toFixed(2).toString().replace(".", ",");
    }else if (box1.checked === true && box4.checked === true){
        rangeValue.innerHTML = (prices[value -25] + addon1[value -25]+ addon4[value -25]).toFixed(2).toString().replace(".", ",");
    }else if (box2.checked === true && box3.checked === true){
        rangeValue.innerHTML = (prices[value -25] + addon1[value -25] + addon2[value -25]).toFixed(2).toString().replace(".", ",");
    }else if (box2.checked === true && box4.checked === true){
        rangeValue.innerHTML = (prices[value -25] + addon2[value -25] + addon4[value -25]).toFixed(2).toString().replace(".", ",");
    }else if (box3.checked === true && box4.checked === true  ){
        rangeValue.innerHTML = (prices[value -25] + addon3[value -25] + addon4[value -25]).toFixed(2).toString().replace(".", ",");
    }else if (box1.checked === true){
        rangeValue.innerHTML = (prices[value -25] + addon1[value -25]).toFixed(2).toString().replace(".", ",");
    }else if (box2.checked === true ){
        rangeValue.innerHTML = (prices[value -25] + addon2[value -25]).toFixed(2).toString().replace(".", ",");
    }else if (box3.checked === true){
        rangeValue.innerHTML = (prices[value -25] + addon3[value -25]).toFixed(2).toString().replace(".", ",");
    }else if (box4.checked === true){
        rangeValue.innerHTML = (prices[value -25] + addon4[value -25]).toFixed(2).toString().replace(".", ",");
    } else {
        rangeValue.innerHTML = prices[value -25];
    }
    
}

// Bubble style
function setDataSize(x){
    style.innerHTML = ".range .sliderPeople span::after { width: " + increaseSize(x)  + "px !important; height: " + increaseSize(x);
    style2.innerHTML = ".range .sliderValue span::after { width: " + decreaseSize(x)  + "px !important; height: " + decreaseSize(x) ;
}

function increaseSize(x) { 
    if (x < 50) {
        return 80
    } else if (x < 90) {
        return 81
    } else if (x < 130) {
        return 81.5
    } else if (x < 170) {
        return 82
    } else if (x < 210) {
        return 82.5
    } else if (x < 250) {
        return 83
    } else if (x < 290) {
        return 83.5
    } else if (x < 330) {
        return 84
    } else if (x < 370) {
        return 84.5
    } else if (x < 410) {
        return 85
    } else if (x < 450) {
        return 85.5
    } else if (x < 490) {
        return 86
    } else if (x < 530) {
        return 86.5
    } else if (x < 570) {
        return 87
    } else if (x < 610) {
        return 87.5
    } else if (x < 650) {
        return 88
    } else if (x < 690) {
        return 88.5
    } else if (x < 730) {
        return 89
    } else if (x < 770) {
        return 89.5
    } else if (x < 810) {
        return 90
    } else if (x < 850) {
        return 90
    } else if (x < 890) {
        return 90.5
    } else if (x < 930) {
        return 90.5
    } else if (x < 970) {
        return 91
    }else {
        return 91
    }
}

function decreaseSize(x) {
    if (x < 50) {
        return 80
    } else if (x < 90) {
        return 79
    } else if (x < 130) {
        return 78.5
    } else if (x < 170) {
        return 78
    } else if (x < 210) {
        return 77.5
    } else if (x < 250) {
        return 77
    } else if (x < 290) {
        return 76.5
    } else if (x < 330) {
        return 76
    } else if (x < 370) {
        return 75.5
    } else if (x < 410) {
        return 75
    } else if (x < 450) {
        return 74.5
    } else if (x < 490) {
        return 74
    } else if (x < 530) {
        return 73.5
    } else if (x < 570) {
        return 73
    } else if (x < 610) {
        return 72.5
    } else if (x < 650) {
        return 72
    } else if (x < 690) {
        return 71.5
    } else if (x < 730) {
        return 71
    } else if (x < 770) {
        return 70.5
    } else if (x < 810) {
        return 70
    } else if (x < 850) {
        return 69.5
    } else if (x < 890) {
        return 69
    } else if (x < 930) {
        return 68.5
    } else if (x < 970) {
        return 68
    }else {
        return 67.5
    }
}


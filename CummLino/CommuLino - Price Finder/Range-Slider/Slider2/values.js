// Slider price
const range = (start, stop, step) => Array.from({ length: (stop - start) / step + 1}, (_, i) => start + (i * step));
let rangePrices = range(25, 1000, 1);

const prices = [];

rangePrices.forEach((rangeValue, i)=> {
    if(rangeValue === 25) {
        prices.push(299)
    } else if (rangeValue < 51) {
         prices.push(prices[i-1] + 6)
    } else if (rangeValue < 251){
        prices.push(prices[i-1] + 4)
    } else if (rangeValue < 501) {
        prices.push(prices[i-1] + 3)
    } else {
        prices.push(prices[i-1] + 2)
    }
});


// Addon 1 Onobarding
const addon1 = [];

rangePrices.forEach((rangeValue, i)=> {
    if(rangeValue === 25) {
        addon1.push(17.5)
    } else if (rangeValue < 51) {
        addon1.push(parseFloat((addon1[i-1] + 0.6).toFixed(2)))
    } else if (rangeValue < 251){
        addon1.push(parseFloat((addon1[i-1] + 0.5).toFixed(2)))
    } else if (rangeValue < 501) {
        addon1.push(parseFloat((addon1[i-1] + 0.4).toFixed(2)))
    } else {
        addon1.push(parseFloat((addon1[i-1] + 0.3).toFixed(2)))
    }
});

// Addon 2 Video
const addon2 = [];

rangePrices.forEach((rangeValue, i)=> {
    if(rangeValue === 25) {
        addon2.push(15)
    } else if (rangeValue < 51) {
        addon2.push(parseFloat((addon2[i-1] + 0.5).toFixed(2)))
    } else if (rangeValue < 251){
        addon2.push(parseFloat((addon2[i-1] + 0.4).toFixed(2)))
    } else if (rangeValue < 501) {
        addon2.push(parseFloat((addon2[i-1] + 0.3).toFixed(2)))
    } else {
        addon2.push(parseFloat((addon2[i-1] + 0.2).toFixed(2)))
    }
});

// Addon 3 Speiseplan
const addon3 = [];

rangePrices.forEach((rangeValue, i)=> {
    if(rangeValue === 25) {
        addon3.push(12.5)
    } else if (rangeValue < 51) {
        addon3.push(parseFloat((addon3[i-1] + 0.4).toFixed(2)))
    } else if (rangeValue < 251){
        addon3.push(parseFloat((addon3[i-1] + 0.3).toFixed(2)))
    } else if (rangeValue < 501) {
        addon3.push(parseFloat((addon3[i-1] + 0.2).toFixed(2)))
    } else {
        addon3.push(parseFloat((addon3[i-1] + 0.1).toFixed(2)))
    }
});

// Addon 4 Weboberfläche
const addon4 = [];

rangePrices.forEach((rangeValue, i)=> {
    if(rangeValue === 25) {
        addon4.push(0)
    } else if (rangeValue < 51) {
        addon4.push(parseFloat((addon4[i-1] + 0).toFixed(2)))
    } else if (rangeValue < 251){
        addon4.push(parseFloat((addon4[i-1] + 0).toFixed(2)))
    } else if (rangeValue < 501) {
        addon4.push(parseFloat((addon4[i-1] + 0).toFixed(2)))
    } else {
        addon4.push(parseFloat((addon4[i-1] + 0).toFixed(2)))
    }
});




// El eventlistener crec que ha de ser un change. Pot ser la funció ha d'anar relacionada amb css.
// Definim unes variables. Amb getElementByClassName no em fucionava.

var slider = document.querySelector('#myRange');
var output = document.querySelector("#value-range");
let rangeValue = document.querySelector('#final-price');
const boxesContainer = document.querySelector('.price-boxes__container')
let total = 200;

// Range
output.innerHTML = slider.value;
rangeValue.innerHTML = total + parseInt(slider.value);

slider.oninput = function () {
    output.innerHTML = this.value;
    rangeValue.innerHTML = total + parseInt(this.value)*2;
}



// Imprimir el valor del range en l'output
loadevent ();

function loadevent (){
    boxesContainer.addEventListener('change', boxesClick);
    slider.addEventListener("mousemove", colorRange)
}

// Funció que en clicar el box, identifica el box que és i el valor que té
function boxesClick (e) {
    if (e.target.classList.contains('checkbox')){
        const price = parseInt(e.target.getAttribute('value'))
        if (e.target.checked === true) {
            total += price;
            console.log(total)
        }else {
            total -= price;
            console.log(total);
            
        }
        rangeValue.innerHTML = total;
    };
}

function colorRange() {
    var x = slider.value;
    var color = 'linear-gradient(120deg, rgb(0,183,255) %, rgb(0,89,255)' + x + '%)';
    slider.style.background = color;
}







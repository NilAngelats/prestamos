let userScore = 0;
let computerScore = 0;
const userScore_span = document.getElementById("user-score");
const computerScore_span = document.getElementById("computer-score");
const scoreBoard_div = document.querySelector(".score-board");
const resultBoard_p = document.querySelector(".result > p");
const rock_div = document.getElementById("r");
const paper_div = document.getElementById("p");
const scissor_div = document.getElementById("s");

function getComputerChoice() {
    const choices = ["r", "p", "s"];
    const randomNumber = Math.floor(Math.random() * 3);
    return choices[randomNumber];
}

function convertToWord (letter) {
    if (letter === "r") return "Rock";
    if (letter === "p") return "Paper";
    return "Scissors";
}

function win(userChoice, computerChoice) {
    const userChoice_div = document.getElementById(userChoice);
    userScore++;
    userScore_span.innerHTML = userScore;
    computerScore_span.innerHTML = computerScore;
    if(userChoice === "r") {resultBoard_p.innerHTML = `${convertToWord(userChoice)} smashes ${convertToWord(computerChoice)}. You win!`;
    } else if (userChoice === "p") {resultBoard_p.innerHTML = `${convertToWord(userChoice)} covers ${convertToWord(computerChoice)}. You win!`;
    } else {resultBoard_p.innerHTML = `${convertToWord(userChoice)} cuts ${convertToWord(computerChoice)}. You win!`;
    };
    userChoice_div.classList.add('green-glow');
    setTimeout(function() { userChoice_div.classList.remove("green-glow")}, 1000);
       
};

function lose(userChoice, computerChoice) {
    const userChoice_div = document.getElementById(userChoice);
    computerScore++;
    userScore_span.innerHTML = userScore;
    computerScore_span.innerHTML = computerScore;
    if(userChoice === "r") {resultBoard_p.innerHTML = `${convertToWord(userChoice)} is covered by ${convertToWord(computerChoice)}. You lose!`;
    } else if(userChoice === "p") {resultBoard_p.innerHTML = `${convertToWord(userChoice)} is cut by ${convertToWord(computerChoice)}. You lose!`;
    } else {resultBoard_p.innerHTML = `${convertToWord(userChoice)} is smashed by ${convertToWord(computerChoice)}. You lose!`;
    };
    userChoice_div.classList.add('red-glow');
    setTimeout(function() { userChoice_div.classList.remove("red-glow")}, 1000);
}

function draw(userChoice, computerChoice) {
    const userChoice_div = document.getElementById(userChoice);
    computerScore_span.innerHTML = computerScore;
    if(userChoice === "r") {resultBoard_p.innerHTML = `${convertToWord(userChoice)} meets another ${convertToWord(computerChoice)}. It wins a FRIEND!`;
    } else if(userChoice === "p") {resultBoard_p.innerHTML = `${convertToWord(userChoice)} meets another ${convertToWord(computerChoice)}. It wins a FRIEND!`;
    } else {resultBoard_p.innerHTML = `${convertToWord(userChoice)} meets another ${convertToWord(computerChoice)}. It wins a FRIEND!`;
    };
    userChoice_div.classList.add('gray-glow');
    setTimeout(function() { userChoice_div.classList.remove("gray-glow")}, 1000);
}

function game(userChoice) {
    const computerChoice = getComputerChoice();
    switch (userChoice + computerChoice) {
        case "rs":
        case "pr":
        case "sp":
            win(userChoice, computerChoice);
            break;
        case "rp":
        case "ps":
        case "sr":
            lose(userChoice, computerChoice);
            break;
        case "rr":
        case "pp":
        case "ss":
            draw(userChoice, computerChoice);
            break;
    }
};

function main() {
    rock_div.addEventListener("click", function() {
        game("r");
    })
    
    paper_div.addEventListener("click", function() {
        game("p");
    })
    
    scissor_div.addEventListener("click", function() {
        game("s");
    })
};



main();






